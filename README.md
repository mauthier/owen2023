# Owen2023

This material is a companion to the paper by Owen et al. (2023)

## Getting started

Download the RLA package

```
remotes::install_gitlab(host = "https://gitlab.univ-lr.fr", 
                        repo = "pelaverse/rla"
                        )
```

## Depository architecture

- [ ] [data] a *.RData* environment with parameters values to carry out the simulations. This environment is actually generated from the code but as it is quite long to run, it was saved for convenience.
- [ ] [output] the various outputs from the simulation to reproduce figures, etc.


## Management Strategy Evaluation (MSE)

To run an MSE on the Belt Sea Harbour Porpoise, please use the script "MSE_mPBR_BeltSeaHarbourPorpoise.r"


## Authors (alphabetical order)
Matthieu Authier, Mathieu Genu, Kylie Owen, Martin Sköld 

## Project status
Completed
