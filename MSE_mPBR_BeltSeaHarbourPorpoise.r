##--------------------------------------------------------------------------------------------------------
## SCRIPT : Determine the Fr value to be used in an mPBR calculation for the 
##          Belt Sea population
##
## Authors : Owen K, Gilles A, Authier M, Balle JD, Carlström J, Genu M, Kyhn LA, 
##           Ramírez-Martínez NC, Nachtsheim D, Siebert U, Sköld M, Teilmann J, 
##           Unger B, Sveegaard S
##
## Last update : 2023-07-19
##
## R version 4.2.2 (2022-10-31 ucrt) -- "Innocent and Trusting"
## Copyright (C) 2022 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

### install RLA version 0.2.0
## visit https://gitlab.univ-lr.fr/pelaverse/rla
remotes::install_gitlab(host = "https://gitlab.univ-lr.fr", 
                        repo = "pelaverse/rla"
                        )

### load libraries
lapply(c("tidyverse", "skimr", "RLA"),
       library, character.only = TRUE
       )

### organize working directory
if(!dir.exists(paste(getwd(), "data", sep = "/"))) {
  dir.create(paste(getwd(), "data", sep = "/"), recursive = TRUE)
}

if(!dir.exists(paste(getwd(), "output", sep = "/"))) {
  dir.create(paste(getwd(), "output", sep = "/"), recursive = TRUE)
}

### clean up
rm(list = ls())

### Inputs for the population demographics
belt_sea_hp <- list(SCANS = data.frame(year = c(1994, 2005, 2012, 2016, 2020),
                                       ## SCANS and MiniSCANS survey abundance estimates
                                       N_hat = c(51660, 27901, 40475, 42324, 17301),
                                       ## SCANS and MiniSCANS survey CV estimates
                                       CVs = c(0.3, 0.39, 0.24, 0.3, 0.2)           
                                       ),
                    life_history = list(## carrying capacity based on SCANS I abundance estimate (51660)- important to be in the right order of magnitude
                                        K = 50000,  
                                        L = 22,
                                        phi = c(0.85, 0.87, rep(0.91, 20), 0.0),
                                        maturity = c(0.025, 0.05, 0.10, 0.175, 0.375, 0.50, 0.625, 0.825, 0.925, 0.95, 0.975, 0.99, rep(1.0, 11)),
                                        eta = c(2, 2, rep(1, 21)),
                                        sexratio = c(1.0, 1.1), 
                                        sexbias = c(1.0, 1.2)
                                        )
                    )

### Inputs for simulations
n_sim <- 1e5
param <- data.frame(seed = sample.int(1e6, size = n_sim, replace = FALSE),
                    ## based on robustness trials in Genu et al 2022 (Table 3)
                    MNPL = sample(c(0.45, 0.5, 0.7), n_sim, replace = TRUE),           
                    cv_byc = round(runif(n_sim, 0.05, 0.50), 3),
                    cv_env = round(runif(n_sim, 0.00, 0.20), 3),
                    ## based on highest and lowest MiniSCANS/SCANS surveys
                    cv_obs = sample(c(0.2, 0.4), n_sim, replace = TRUE)  ,
                    MNP = sample(c(1.02, 1.04, 1.06), size = n_sim, replace = TRUE),
                    ## rate of by-catch as a proportion of K 
                    rate = round(runif(n_sim, 0.001, 0.050), 3)
                    )

## use age-disaggregated operating model
system.time(
  sim <- lapply(1:nrow(param),
                function(i) {
                  pellatomlinson_dis(
                    vital_param = vital_param_dis(
                      L = belt_sea_hp$life_history$L,# maximum longevity,
                      K = belt_sea_hp$life_history$K, # carrying capacity
                      # vulnerabilities to bycatch
                      eta = belt_sea_hp$life_history$eta,
                      # survival probabilities
                      phi = belt_sea_hp$life_history$phi,
                      # proportions of mature females in each age class
                      m = belt_sea_hp$life_history$maturity,
                      MNP = param$MNP[i],
                      MNPL = param$MNPL[i]
                    ),
                    # series of catches
                    removals = bycatch_rw(n = 60,
                                          K = belt_sea_hp$life_history$K,
                                          rate = param$rate[i],
                                          cv = param$cv_byc[i],
                                          seed_id = param$seed[i]
                                          ),
                    CV = param$cv_obs[i],
                    # CV of environmental stochasticity on birth rate
                    CV_env = param$cv_env[i],
                    # scans survey
                    scans = c(34, 45, 52, 56, 60),
                    verbose = FALSE,
                    # for reproducibility purposes
                    seed_id = param$seed[i]
                    )
                  }
                )
  )

# on a laptop HP Elitebook 840 G8
# 11th Gen Intel(R) Core(TM) i5-1145G7 @ 2.60GHz   2.61 GHz
# RAM: 32,0 Go
# utilisateur     système      écoulé 
#    16661.88      275.67    17031.50 

### identify scenarios with the desired level of depletion
all_scenarios_belt_sea_hp <- param %>%
  mutate(current_depletion = do.call('c',
                                     lapply(sim,
                                            function(l) {
                                              l$depletion[length(l$depletion)]
                                            })
                                     ),
         sim = 1:n()
         ) %>%
  filter(current_depletion >= 0.3,
         current_depletion <= 0.7
         ) %>%
  mutate(level_mnp = factor(MNP, levels = c(1.02, 1.04, 1.06)),
         level_mnpl = factor(MNPL, levels = c(0.45, 0.5, 0.7)),
         level_depletion = cut(current_depletion, breaks = seq(0.3, 0.7, 0.2))
         ) %>%
  group_by(level_mnp, level_mnpl, level_depletion, cv_obs) %>%
  slice_sample(n = 3e2) %>%
  ungroup()

## quick summary
all_scenarios_belt_sea_hp %>%
  skimr::skim()

## save: accept/reject step is costly
save(param,
     file = "./data/param.RData"
     )
save(all_scenarios_belt_sea_hp,
     file = "./output/all_scenarios_belt_sea_hp.RData"
     )
all_scenarios_belt_sea_hp %>%
  write.csv(.,
            "./output/all_scenarios_belt_sea_hp.csv", 
            row.names = TRUE
            )

### Management Strategy Evaluation
##  Robustness trials 0-6 and 9-10 of Genu et al. (2021)
trials <- data.frame(scenario = c("0", "1", "2", "3", "4", "5", "6", "9", "10"),
                     cv_surv = c(1, 1, 1, 1, 2, 1, 1, 1, 1),
                     cv_byc = c(0.3, 0.3, 0.3, 0.3, 0.3, 1.2, 0.3, 0.3, 0.3),
                     f_surv = c(6, 6, 6, 6, 6, 6, 10, 6, 6),
                     b_abund = c(1, 1, 2, 1, 1, 1, 1, 1, 1),
                     b_byc = c(1, 2, 1, 1, 1, 1, 1, 1, 1),
                     b_Rmax = c(1, 1, 1, 0.5, 1, 1, 1, 1, 1),
                     cata = c(0, 0, 0, 0, 0, 0, 0, 0.1, 0),
                     Kfinal = c(1, 1, 1, 1, 1, 1, 1, 1, 0.5)
                     )

# all_scenarios_belt_sea_hp <- read.csv("./output/all_scenarios_belt_sea_hp.csv", 
#                                       header = TRUE
#                                       )


# wrapper function to run the MSE, 
# it is calling some of the objects in the global env.
# and it takes some time to finish
run_scenarios <- function(mnp, 
                          mnpl,
                          coef_var,
                          recovery_factor = seq(0.1, 1, 0.1)
                          ) {
  # subset
  inputs <- all_scenarios_belt_sea_hp %>% 
    filter(MNP == mnp, 
           MNPL == mnpl, 
           cv_obs == coef_var
           )
  
  # before management
  sim <- lapply(1:nrow(inputs),
                function(i) {
                  pellatomlinson_dis(
                    vital_param = vital_param_dis(
                      L = belt_sea_hp$life_history$L,# maximum longevity,
                      K = belt_sea_hp$life_history$K, # carrying capacity
                      # vulnerabilities to bycatch
                      eta = belt_sea_hp$life_history$eta,
                      # survival probabilities
                      phi = belt_sea_hp$life_history$phi,
                      # proportions of mature females in each age class
                      m = belt_sea_hp$life_history$maturity,
                      MNP = mnp,
                      MNPL = mnpl
                    ),
                    # series of catches
                    removals = bycatch_rw(n = 60,
                                          K = belt_sea_hp$life_history$K,
                                          rate = inputs$rate[i],
                                          cv = inputs$cv_byc[i],
                                          seed_id = inputs$seed[i]
                                          ),
                    CV = coef_var,
                    # CV of environmental stochasticity on birth rate
                    CV_env = inputs$cv_env[i],
                    # scans survey
                    scans = c(34, 45, 52, 56, 60),
                    verbose = FALSE,
                    # for reproducibility purposes
                    seed_id = inputs$seed[i]
                    )
                  }
                )
  
  # plot population trajectory
  trajectory <- map(.x = 1:length(sim),
                    .f = function(i) { 
                      data.frame(time = 0:(length(sim[[i]]$depletion) - 1),
                                 depletion = sim[[i]]$depletion,
                                 seed = rep(sim[[i]]$seed_id, length(sim[[i]]$depletion))
                                 ) %>% 
                        left_join(inputs,
                                  by = "seed"
                                  )
                      }
                    ) %>%
    list_rbind()

  A <- trajectory %>% 
    ggplot(aes(x = time, y = depletion, group = seed)) +
    geom_line(color = "midnightblue", alpha = 0.1) +
    scale_x_continuous(name = "# years",
                       breaks = seq(0, 600, 50)
                       ) +
    scale_y_continuous(name = "Depletion (as % of K)", 
                       breaks = seq(0.0, 2.0, 0.2), 
                       labels = 100 * seq(0.0, 2.0, 0.2)
                       ) +
    facet_grid(MNP ~ MNPL) +
    theme_bw()

  # management with PBR control rule
  tuning <- function(tuning_factor) {
    res <- lapply(sim, function(x) {
      out <- map(.x = 1:nrow(trials),
                 .f = function(i) {
                   # increased survey variability in scenario
                   x$CV <- x$CV * trials$cv_surv[i]
                   # population dynamics
                   dd <- pbr_nouveau(op_model = x,
                                     frequency = 6,
                                     horizon = 100,
                                     F_r = tuning_factor,
                                     # bycatch-related parameters
                                     bycatch_variation_cv = trials$cv_byc[i],
                                     distribution = "truncnorm",
                                     # robustness checks
                                     bias_abund = trials$b_abund[i],
                                     Ktrend = trials$Kfinal[i],
                                     catastrophe = trials$cata[i],
                                     bias_byc = trials$b_byc[i],
                                     random = FALSE,
                                     averaging = FALSE
                                     )$depletion %>%
                     select(time, depletion) %>%
                     mutate(trial = trials$scenario[i],
                            seed = x$seed_id,
                            MNP = mnp,
                            MNPL = mnpl,
                            CV = coef_var,
                            F_r = tuning_factor
                            )
                   return(dd)
                 }
                 ) %>%
        list_rbind()
      
      return(out)
    })
    
    res <- do.call('rbind', res) %>%
      mutate(trial = factor(trial, 
                            levels = c("0", "1", "2", "3", "4", "5", "6", "9", "10")
                            )
             )
    
    return(res)
  }
  
  ### tuning recovery factor
  results <- map(.x = recovery_factor,
                 .f = tuning
                 ) %>%
    list_rbind()
  
  # plot population trajectory during management period
  B <- results %>% 
    ggplot(aes(x = time, y = depletion, group = seed)) +
    geom_line(color = "midnightblue", alpha = 0.05) +
    scale_x_continuous(name = "# years",
                       breaks = seq(0, 100, 25)
                       ) +
    scale_y_continuous(name = "Depletion (as % of K)", 
                       breaks = seq(0.0, 2.0, 0.25), 
                       labels = 100 * seq(0.0, 2.0, 0.25)
                       ) +
    facet_grid(F_r ~ trial) +
    theme_bw()
  
  out <- list(inputs = inputs,
              output = results,
              plot_before = A,
              plot_mse = B
              )
  return(out)
}

### please be patient
comb1 <- run_scenarios(mnp = 1.04, 
                       mnpl = 0.5,
                       coef_var = 0.2
                       )

### figure for all trials
comb1$output %>% 
  filter(time == 100) %>%
  group_by(trial, F_r) %>% 
  summarise(P = mean(depletion >= 0.8)) %>% 
  ggplot(aes(x = F_r, y = P)) + 
  geom_line() + 
  geom_point() +
  geom_hline(yintercept = 0.8, col="tomato") + 
  labs(x = "Recovery factor", y = "Probability") + 
  facet_wrap(~ trial, ncol = 3) +
  theme_bw()

### base case 
comb1$output %>% 
  filter(time == 100,
         trial == "0"
         ) %>%
  group_by(F_r) %>% 
  summarise(P = mean(depletion >= 0.8)) %>% 
  ggplot(aes(x = F_r, y = P)) + 
  geom_line() + 
  geom_point() +
  geom_hline(yintercept = 0.8, col="tomato") + 
  labs(x = "Recovery factor", y = "Probability") + 
  theme_bw()

### contrast with a lower MNPL
trials <- trials %>%
  filter(scenario == "0")

comb2 <- run_scenarios(mnp = 1.04, 
                       mnpl = 0.45,
                       coef_var = 0.2
                       )

comb2$output %>% 
  filter(time == 100,
         trial == "0"
         ) %>%
  group_by(F_r) %>% 
  summarise(P = mean(depletion >= 0.8)) %>% 
  ggplot(aes(x = F_r, y = P)) + 
  geom_line() + 
  geom_point() +
  geom_hline(yintercept = 0.8, col="tomato") + 
  labs(x = "Recovery factor", y = "Probability") + 
  theme_bw()

# show the population trajectories
comb1$output %>% 
  filter(F_r == 0.2,
         trial == "0"
         ) %>%
  ggplot() +
  geom_line(aes(x = time, y = depletion, group = seed),
            color = "midnightblue", alpha = 0.1
            ) +
  geom_smooth(aes(x = time, y = depletion), method = "loess", se = FALSE, 
              linetype = "dotted", color = "midnightblue"
              ) +
  scale_x_continuous(name = "# years",
                     breaks = seq(0, 100, 10)
                     ) +
  scale_y_continuous(name = "Depletion (as % of K)", 
                     breaks = seq(0.0, 2.0, 0.20), 
                     labels = 100 * seq(0.0, 2.0, 0.20)
                     ) +
  geom_hline(yintercept = 0.8, col = "tomato", linetype = "dashed") + 
  theme_bw()

# with lower MNPL
comb2$output %>% 
  filter(F_r == 0.1,
         trial == "0"
         ) %>%
  ggplot() +
  geom_line(aes(x = time, y = depletion, group = seed),
            color = "midnightblue", alpha = 0.1
            ) +
  geom_smooth(aes(x = time, y = depletion), method = "loess", se = FALSE, 
              linetype = "dotted", color = "midnightblue"
              ) +
  scale_x_continuous(name = "# years",
                     breaks = seq(0, 100, 10)
                     ) +
  scale_y_continuous(name = "Depletion (as % of K)", 
                     breaks = seq(0.0, 2.0, 0.20), 
                     labels = 100 * seq(0.0, 2.0, 0.20)
                     ) +
  geom_hline(yintercept = 0.8, col = "tomato", linetype = "dashed") + 
  theme_bw()

# save simulations
save(list = c("comb1", "comb2"), file = "./output/simulations.RData")
